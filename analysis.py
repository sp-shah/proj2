import numpy as np
import matplotlib.pyplot as plt
import sys

c = 3.e10
G = 6.67e-8/c**2
Msol = 2.e33
au = 3.e18

e = 0.98 #eccentricity
Mb = 4.3e6*Msol
r0 = Mb*G
rs = 2.*r0
a = 100.*rs
print(a/au)
b = a*np.sqrt(1-e**2)
T = np.sqrt(a**3*4*np.pi**2/r0)
ra = a*(1+e)
rp = a*(1-e)


E=-r0/(2.*a) #Energy
L=np.sqrt(a*r0*(1-e**2)) #Angular momentum
Ls=L/np.sqrt(1-((r0*(3+e**2))/(a*(1-e**2)))) #Angular Momentum in Schwarzchild metric
Es=(1.+2.*E*(1.-r0/(a-r0*(3.+e**2)/(1-e**2.)))) #Energy in Schwarzchild metric

 # Constrainting the parameters
theta = np.pi/2
utheta = 0


############################################################################################
# GR ORBITS

def f(vect):
       
    t = vect[0]
    r = vect[1]
    phi = vect[2]
    ut = vect[3]
    ur = vect[4]
    uphi = vect[5]

    tdot = ut
    rdot = ur
    phidot = uphi
    utdot = -(2*r0*ur*ut)/(r*(r-2*r0))
    urdot = (-((r0*(r-2*r0)*ut**2)/r**3)+ ((r0*ur**2)/(r*(r-2*r0))) + (r-2*r0)*(np.sin(theta))**2*(uphi**2))
    uphidot = -(2*uphi*ur/r)
      
    dot = np.array([tdot, rdot, phidot, utdot, urdot, uphidot])

    return dot


def adap_timestep(init, h):
    initcp = np.copy(init)
    initc = np.copy(init)
    r_ar1, phi_ar1 = main(initcp,h,2*h,0)
    r_ar2, phi_ar2 = main(initc, 2*h, 2*h,0)

    phimag = np.abs(phi_ar2[-1]-phi_ar1[-1])
    delta = 1.e-24
    rho = 30.*h*delta/phimag
    hprime = h*rho**(1/4)

    return hprime
    
    

def runge_kutta(init, h):    
    k1 = h*f(init)
    k2 = h*f(init+k1/2)
    k3 = h*f(init+k2/2)
    k4 = h*f(init+k3)
    return init+(k1+2*k2+2*k3+k4)/6



def main(init,h,final_time,test):
    r = init[1]
    phi = init[2]
    time = 0
    r_ar = [r]
    phi_ar = [phi]
        
    while time<final_time:
        #if test:
         #   hprime = adap_timestep(init,h)
          #  h = hprime
           
        init = runge_kutta(init, h)
        r_ar.append(init[1])
        phi_ar.append(init[2])
        
        time += h
        
    return r_ar, phi_ar


t = 0
r = rp
phi = 0
tdot = np.sqrt(Es)/(1-2*r0/r)
phidot = Ls/r**2
rdot = np.sqrt(Es - (1-2*r0/r)*(Ls**2/r**2 +1))
init = [t, r, phi, tdot, rdot, phidot]


r_ar, phi_ar = main(init,T/1000,6.*T,1)
phi_ar = np.array(phi_ar)

plt.plot(r_ar)
plt.show()

#plt.plot(phi_ar%(2*np.pi))
#plt.show()

r_ar = np.array(r_ar)

w = np.where(np.abs(r_ar-ra)/ra <= 1.e-6)
ind = w[0]
print(ind)
#ind = np.delete(ind,5)
#ind = np.delete(ind,4)
print(ind)
shift = np.zeros(5)
for i in range(5):
    shift[i] = phi_ar[ind[i+1]]-phi_ar[ind[i]]

shift_avg = np.mean(shift)

print('Calculated Shift',shift_avg)
shift_exp = 24*(np.pi**3)*(a**2)/((T**2)*(1-e**2))
print('Expected shift',shift_exp)

plt.polar(phi_ar, r_ar)
plt.title('Sagittarius A*, a = 4.25e-6 [10*Rs] pc, e = 0.5')

#######################################################################################
# NEWTONIAN ORBIT

def fn(init):
    theta = init[0]
    r = init[1]
   
    thetadot = (2*np.pi*a*b/(T*r**2))
    rdot =  a*e*(1-e**2)*np.sin(theta)*thetadot/(1+e*np.cos(theta))**2
    
    return np.array([thetadot, rdot])

def runge_kutta_n(init,h):
    k1 = h*fn(init)
    k2 = h*fn(init+k1/2)
    k3 = h*fn(init+k2/2)
    k4 = h*fn(init+k3)
    return init+(k1+2*k2+2*k3+k4)/6

def main_n():
    
    theta = 0
    r = rp
    
    init = [theta, r]
    rn_ar = [r]
    theta_ar = [theta]

    time = 0
    h = T/1000
    
    while time<1*T:
        init = runge_kutta_n(init,h)
        theta_ar.append(init[0])
        rn_ar.append(init[1])
        time += h
    return rn_ar, theta_ar
    
rn_ar, theta_ar = main_n()

plt.polar(theta_ar, rn_ar)
plt.show()



#####################################
# Initial conditions set by a, e, and T
'''
e = 0.5
a = 0.05 #semi-major axis cm
b = a*np.sqrt(1-e**2)
T = 2.*np.pi #orbital period in seconds
r0 = a**3*(2*np.pi/T)**2 #r0=GM
ra = a*(1+e)
rp = a*(1-e)

print(r0)
print(a)
sys.exit()

A1 = (1-2*r0/ra)
A2 = (1-2*r0/rp)
Ls = np.sqrt((A1-A2)/((A2/rp**2) - (A1/ra**2)))
print(Ls)
'''
######################################################


