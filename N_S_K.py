import numpy as np
import matplotlib.pyplot as plt
from math import sqrt
#import scipy.constants as sconst

e=0.5 #the eccentricity
a=0.25 #the semi-major axis
T=2*np.pi #the orbital period
#G = sconst.G # Gravitational constant
G = 1 # since in our metric c =1 
Q = 1e-10 # charge
#Q = 3.4

r0=a**3*(2*np.pi/T)**2 #r0=GM
rp=a*(1+e) #the aphelion
rm=a*(1-e) #the perihelion
rq=G*(Q**2)

E=-r0/(2*a) #Energy
L=sqrt(a*r0*(1-e**2)) #Angular momentum

Ls=L/sqrt(1-r0*(3+e**2)/(1-e**2)/a) #Energy in Schwarzchild metric
Es=1+2*E*(1-r0/(a-r0*(3+e**2)/(1-e**2))) #Angular momentum in Schwarzchild metric


A1 = 1 - 2*r0/rp + rq**2/rp**2
A2 = 1 - 2*r0/rm + rq**2/rm**2
Lq = sqrt((A2-A1)/(A1/rp**2 - A2/rm**2)) #Energy in Schwarzchild metric
Eq = A1*(Lq**2/rp**2 + 1) #Angular momentum in Schwarzchild metric


def f(pol):
    r=pol[0]
    global p
    if r>rp:
        r=2*rp-r    
        p=-p
    elif r<rm:
        r=2*rm-r
        p=-p
    dr=sqrt(2*E+2*r0/r-(L/r)**2)
    return np.array([p*dr,L/r**2])

def rk4(pol,h):
    k1=h*f(pol)
    k2=h*f(pol+k1/2)
    k3=h*f(pol+k2/2)
    k4=h*f(pol+k3)
    return pol+(k1+2*k2+2*k3+k4)/6

def fs(pol):
    r=pol[0]
    global p
    if r>rp:
        r=2*rp-r    
        p=-1
    elif r<rm:
        r=2*rm-r
        p=1
    dr=sqrt(Es-(1-2*r0/r)*((Ls/r)**2+1)) # this is what needs to be changed for charged black hole
    return np.array([p*dr,Ls/r**2])

def rk4s(pol,h):
    k1=h*fs(pol)
    k2=h*fs(pol+k1/2)
    k3=h*fs(pol+k2/2)
    k4=h*fs(pol+k3)
    return pol+(k1+2*k2+2*k3+k4)/6

def fq(pol):
    r=pol[0]
    global p
    if r>rp:
        r=2*rp-r    
        p=-1
    elif r<rm:
        r=2*rm-r
        p=1
    dr=sqrt(Eq - (1 - 2*r0/r + rq/r**2)*((Lq/r)**2+1)) # this is what needs to be changed for charged black hole
    return np.array([p*dr,Ls/r**2])

def rk4q(pol,h):
    k1=h*fq(pol)
    k2=h*fq(pol+k1/2)
    k3=h*fq(pol+k2/2)
    k4=h*fq(pol+k3)
    return pol+(k1+2*k2+2*k3+k4)/6

#initial conditions
t=0
p=1
pol=np.array([1.00001*rm,0])
Pol=np.array([pol])
h=T/1000 #time step
while t<6*T: #the main loop
    pol=rk4(pol,h)
    Pol=np.append(Pol,np.array([pol]),axis=0)
    t+=h
plt.polar(Pol[:,1],Pol[:,0],c = 'b',label = "Newtonian")

t=0
p=1
pols=np.array([1.00001*rm,0])
Pols=np.array([pols])
while t<6*T:
    pols=rk4s(pols,h)
    Pols=np.append(Pols,np.array([pols]),axis=0)
    t+=h
plt.polar(Pols[:,1],Pols[:,0],c = 'r',label = "Schwarzschild Metric")

t=0
p=1
polq = np.array([1.00001*rm,0])
Polq = np.array([polq])
h=T/1000 #time step
while t<6*T: #the main loop
    polq=rk4q(polq,h)
    Polq=np.append(Polq,np.array([polq]),axis=0)
    t+=h
plt.polar(Polq[:,1],Polq[:,0], c = 'k', label = "Reissner-Norstr¨om Metric")
plt.legend(loc = 'upper right')
plt.show()
