import numpy as np
import matplotlib.pyplot as plt
import sys

#c = 3e10
#G = 6.67e-8/c**2

e = 0.5 #eccentricity
a = 0.25 #semi-major axis cm
b = a*np.sqrt(1-e**2)
T = 2*np.pi #orbital period in seconds
r0 = a**3*(2*np.pi/T)**2 #r0=GM
ra = a*(1+e)
rp = a*(1-e)


E=-r0/(2*a) #Energy
L=np.sqrt(a*r0*(1-e**2)) #Angular momentum
Ls=L/np.sqrt(1-r0*(3+e**2)/(1-e**2)/a) #Energy in Schwarzchild metric
Es=(1+2*E*(1-r0/(a-r0*(3+e**2)/(1-e**2)))) #Angular momentum in Schwarzchild metric

############################################################################################
# GR ORBITS


def f(vect):
    # Constrainting the parameters
    theta = np.pi/2
    utheta = 0
    
    t = vect[0]
    r = vect[1]
    phi = vect[2]
    ut = vect[3]
    ur = vect[4]
    uphi = vect[5]

    tdot = ut
    rdot = ur
    phidot = uphi
    utdot = -(2*r0*ur*ut)/(r*(r-2*r0))
    urdot = -((r0*(r-2*r0)*ut**2)/r**3)+ ((r0*ur**2)/(r*(r-2*r0))) + (r-2*r0)*(np.sin(theta))**2*(uphi**2)
    uphidot = -(2*uphi*ur/r)
                                                                           

    dot = np.array([tdot, rdot, phidot, utdot, urdot, uphidot])

    return dot

def runge_kutta(init, h):    
    k1 = h*f(init)
    k2 = h*f(init+k1/2)
    k3 = h*f(init+k2/2)
    k4 = h*f(init+k3)
    return init+(k1+2*k2+2*k3+k4)/6


def main():
    time = 0
    h = T/1000
    
    t = 0
    r = rp
    phi = 0

    tdot = np.sqrt(Es)/(1-2*r0/r)
    phidot = Ls/r**2
    rdot = np.sqrt(Es - (1-2*r0/r)*(Ls**2/r**2 +1))

    init = [t, r, phi, tdot, rdot, phidot]

    r_ar = [r]
    phi_ar = [phi]

    while time<6*T:
        init = runge_kutta(init, h)
        r_ar.append(init[1])
        phi_ar.append(init[2])
        time += h
        
    return r_ar, phi_ar

r_ar, phi_ar = main()
plt.polar(phi_ar, r_ar)
#plt.show()

#######################################################################################
# NEWTONIAN ORBIT

def fn(init):
    theta = init[0]
    r = init[1]
   
    thetadot = (2*np.pi*a*b/(T*r**2))
    rdot =  r*a*np.sin(theta)*thetadot/(1+e*np.cos(theta))
    
    return np.array([thetadot, rdot])

def runge_kutta_n(init,h):
    k1 = h*fn(init)
    k2 = h*fn(init+k1/2)
    k3 = h*fn(init+k2/2)
    k4 = h*fn(init+k3)
    return init+(k1+2*k2+2*k3+k4)/6

def main_n():
    
    theta = 0
    r = rp
    
    init = [theta, r]
    rn_ar = [r]
    theta_ar = [theta]

    time = 0
    h = T/1000
    
    while time<6*T:
        init = runge_kutta_n(init,h)
        theta_ar.append(init[0])
        rn_ar.append(init[1])
        time += h
    return rn_ar, theta_ar
    
rn_ar, theta_ar = main_n()

plt.polar(theta_ar, rn_ar)
plt.show()
